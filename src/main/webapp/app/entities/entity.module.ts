import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { BmwsaImageModule } from './image/image.module';
import { BmwsaImageLikeModule } from './image-like/image-like.module';
import { BmwsaTagModule } from './tag/tag.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        BmwsaImageModule,
        BmwsaImageLikeModule,
        BmwsaTagModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BmwsaEntityModule {}
