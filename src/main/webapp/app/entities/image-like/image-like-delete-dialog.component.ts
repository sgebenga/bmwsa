import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ImageLike } from './image-like.model';
import { ImageLikePopupService } from './image-like-popup.service';
import { ImageLikeService } from './image-like.service';

@Component({
    selector: 'jhi-image-like-delete-dialog',
    templateUrl: './image-like-delete-dialog.component.html'
})
export class ImageLikeDeleteDialogComponent {

    imageLike: ImageLike;

    constructor(
        private imageLikeService: ImageLikeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.imageLikeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'imageLikeListModification',
                content: 'Deleted an imageLike'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-image-like-delete-popup',
    template: ''
})
export class ImageLikeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private imageLikePopupService: ImageLikePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.imageLikePopupService
                .open(ImageLikeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
