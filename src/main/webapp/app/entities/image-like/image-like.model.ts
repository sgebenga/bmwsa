import { BaseEntity } from './../../shared';

export class ImageLike implements BaseEntity {
    constructor(
        public id?: number,
        public userId?: number,
        public imageId?: number,
    ) {
    }
}
