import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ImageLike } from './image-like.model';
import { ImageLikePopupService } from './image-like-popup.service';
import { ImageLikeService } from './image-like.service';
import { User, UserService } from '../../shared';
import { Image, ImageService } from '../image';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-image-like-dialog',
    templateUrl: './image-like-dialog.component.html'
})
export class ImageLikeDialogComponent implements OnInit {

    imageLike: ImageLike;
    isSaving: boolean;

    users: User[];

    images: Image[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private imageLikeService: ImageLikeService,
        private userService: UserService,
        private imageService: ImageService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.userService.query()
            .subscribe((res: ResponseWrapper) => { this.users = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.imageService.query()
            .subscribe((res: ResponseWrapper) => { this.images = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.imageLike.id !== undefined) {
            this.subscribeToSaveResponse(
                this.imageLikeService.update(this.imageLike));
        } else {
            this.subscribeToSaveResponse(
                this.imageLikeService.create(this.imageLike));
        }
    }

    private subscribeToSaveResponse(result: Observable<ImageLike>) {
        result.subscribe((res: ImageLike) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: ImageLike) {
        this.eventManager.broadcast({ name: 'imageLikeListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }

    trackImageById(index: number, item: Image) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-image-like-popup',
    template: ''
})
export class ImageLikePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private imageLikePopupService: ImageLikePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.imageLikePopupService
                    .open(ImageLikeDialogComponent as Component, params['id']);
            } else {
                this.imageLikePopupService
                    .open(ImageLikeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
