import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BmwsaSharedModule } from '../../shared';
import { BmwsaAdminModule } from '../../admin/admin.module';
import {
    ImageLikeService,
    ImageLikePopupService,
    ImageLikeComponent,
    ImageLikeDetailComponent,
    ImageLikeDialogComponent,
    ImageLikePopupComponent,
    ImageLikeDeletePopupComponent,
    ImageLikeDeleteDialogComponent,
    imageLikeRoute,
    imageLikePopupRoute,
    ImageLikeResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...imageLikeRoute,
    ...imageLikePopupRoute,
];

@NgModule({
    imports: [
        BmwsaSharedModule,
        BmwsaAdminModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        ImageLikeComponent,
        ImageLikeDetailComponent,
        ImageLikeDialogComponent,
        ImageLikeDeleteDialogComponent,
        ImageLikePopupComponent,
        ImageLikeDeletePopupComponent,
    ],
    entryComponents: [
        ImageLikeComponent,
        ImageLikeDialogComponent,
        ImageLikePopupComponent,
        ImageLikeDeleteDialogComponent,
        ImageLikeDeletePopupComponent,
    ],
    providers: [
        ImageLikeService,
        ImageLikePopupService,
        ImageLikeResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BmwsaImageLikeModule {}
