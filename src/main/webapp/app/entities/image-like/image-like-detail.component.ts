import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { ImageLike } from './image-like.model';
import { ImageLikeService } from './image-like.service';

@Component({
    selector: 'jhi-image-like-detail',
    templateUrl: './image-like-detail.component.html'
})
export class ImageLikeDetailComponent implements OnInit, OnDestroy {

    imageLike: ImageLike;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private imageLikeService: ImageLikeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInImageLikes();
    }

    load(id) {
        this.imageLikeService.find(id).subscribe((imageLike) => {
            this.imageLike = imageLike;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInImageLikes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'imageLikeListModification',
            (response) => this.load(this.imageLike.id)
        );
    }
}
