import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ImageLikeComponent } from './image-like.component';
import { ImageLikeDetailComponent } from './image-like-detail.component';
import { ImageLikePopupComponent } from './image-like-dialog.component';
import { ImageLikeDeletePopupComponent } from './image-like-delete-dialog.component';

@Injectable()
export class ImageLikeResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const imageLikeRoute: Routes = [
    {
        path: 'image-like',
        component: ImageLikeComponent,
        resolve: {
            'pagingParams': ImageLikeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'bmwsaApp.imageLike.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'image-like/:id',
        component: ImageLikeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'bmwsaApp.imageLike.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'favourite-images',
        component: ImageLikeComponent,
        resolve: {
            'pagingParams': ImageLikeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'bmwsaApp.imageLike.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const imageLikePopupRoute: Routes = [
    {
        path: 'image-like-new',
        component: ImageLikePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'bmwsaApp.imageLike.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'image-like/:id/edit',
        component: ImageLikePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'bmwsaApp.imageLike.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'image-like/:id/delete',
        component: ImageLikeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'bmwsaApp.imageLike.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
