import { BaseEntity } from './../../shared';

export class Image implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public postedDate?: any,
        public contentContentType?: string,
        public content?: any,
        public views?: number,
        public posterId?: number,
        public tags?: BaseEntity[],
        public imageLikes?: BaseEntity[],
    ) {
    }
}
