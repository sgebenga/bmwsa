import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BmwsaSharedModule } from '../shared';

import { HOME_ROUTE, HomeComponent } from './';
import {ImageService} from '../entities/image/image.service';

@NgModule({
    imports: [
        BmwsaSharedModule,
        RouterModule.forRoot([ HOME_ROUTE ], { useHash: true })
    ],
    declarations: [
        HomeComponent,
    ],
    entryComponents: [
    ],
    providers: [
        ImageService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BmwsaHomeModule {}
