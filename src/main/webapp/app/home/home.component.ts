import {Component, OnInit, OnDestroy} from '@angular/core';
import {NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {Subscription} from 'rxjs/Rx';
import {JhiEventManager, JhiParseLinks, JhiAlertService, JhiDataUtils} from 'ng-jhipster';
import {Account, LoginModalService, Principal, ResponseWrapper, ITEMS_PER_PAGE} from '../shared';
import {Image} from '../entities/image/image.model';
import {ImageService} from '../entities/image/image.service'

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: [
        'home.scss'
    ]

})
export class HomeComponent implements OnInit, OnDestroy {
    account: Account;
    modalRef: NgbModalRef;
    eventSubscriber: Subscription;
    images: Image[];
    itemsPerPage: number;
    links: any;
    page: any;
    predicate: any;
    queryCount: any;
    reverse: any;
    totalItems: number;
    initialised: boolean;

    constructor(private principal: Principal,
                private loginModalService: LoginModalService,
                private imageService: ImageService,
                private eventManager: JhiEventManager,
                private parseLinks: JhiParseLinks,
                private jhiAlertService: JhiAlertService,
                private dataUtils: JhiDataUtils) {
        this.images = [];
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.links = {
            last: 0
        };
        this.predicate = 'id';
        this.reverse = true;
        this.initialised = false
    }

    ngOnInit() {
        console.log('**** onInit ***');
        this.principal.identity().then((account) => {
            this.account = account;
            if (this.isAuthenticated()) {
                console.log('**** isAuthenticated ***');
                this.loadAll();
                this.registerChangeInImages();
                this.initialised = true;
            }
        });
        this.registerAuthenticationSuccess();
    }

    ngOnDestroy() {
        console.log('*** onDestroy ***');
        try {
            this.eventManager.destroy(this.eventSubscriber);
            this.reset();
        } catch (e) {        }
    }

    registerAuthenticationSuccess() {
        console.log('*** registerAuthenticationSuccesss ***');
        this.eventManager.subscribe('authenticationSuccess', (message) => {
            this.principal.identity().then((account) => {
                this.account = account;
                if (!this.initialised) {
                    this.loadAll();
                    this.registerChangeInImages();
                    this.initialised = true;
                }
            });
        });
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    isInitialised() {
        return
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    loadAll() {
        this.imageService.query({
            page: this.page,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page) {
        this.page = page;
        this.loadAll();
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    reset() {
        this.page = 0;
        this.images = [];
        this.initialised = false;
        this.loadAll();
    }

    private onSuccess(data, headers) {
        console.log('**** onSuccess ****');
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        for (let i = 0; i < data.length; i++) {
            this.images.push(data[i]);
        }
    }

    private onError(error) {
        console.log('**** onError ****');
        this.jhiAlertService.error(error.message, null, null);
    }

    registerChangeInImages() {
        this.eventSubscriber = this.eventManager.subscribe('imageListModification', (response) => this.reset());
    }
}
