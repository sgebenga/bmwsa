package bmwsa.repository;

import bmwsa.domain.Image;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Image entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ImageRepository extends JpaRepository<Image, Long> {

    @Query("select image from Image image where image.poster.login = ?#{principal.username}")
    List<Image> findByPosterIsCurrentUser();
    @Query("select distinct image from Image image left join fetch image.tags")
    List<Image> findAllWithEagerRelationships();

    @Query("select image from Image image left join fetch image.tags where image.id =:id")
    Image findOneWithEagerRelationships(@Param("id") Long id);

}
