package bmwsa.repository;

import bmwsa.domain.ImageLike;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the ImageLike entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ImageLikeRepository extends JpaRepository<ImageLike, Long> {

    @Query("select image_like from ImageLike image_like where image_like.user.login = ?#{principal.username}")
    List<ImageLike> findByUserIsCurrentUser();

}
