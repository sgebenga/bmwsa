package bmwsa.service.impl;

import bmwsa.service.ImageLikeService;
import bmwsa.domain.ImageLike;
import bmwsa.repository.ImageLikeRepository;
import bmwsa.service.dto.ImageLikeDTO;
import bmwsa.service.mapper.ImageLikeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing ImageLike.
 */
@Service
@Transactional
public class ImageLikeServiceImpl implements ImageLikeService{

    private final Logger log = LoggerFactory.getLogger(ImageLikeServiceImpl.class);

    private final ImageLikeRepository imageLikeRepository;

    private final ImageLikeMapper imageLikeMapper;

    public ImageLikeServiceImpl(ImageLikeRepository imageLikeRepository, ImageLikeMapper imageLikeMapper) {
        this.imageLikeRepository = imageLikeRepository;
        this.imageLikeMapper = imageLikeMapper;
    }

    /**
     * Save a imageLike.
     *
     * @param imageLikeDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ImageLikeDTO save(ImageLikeDTO imageLikeDTO) {
        log.debug("Request to save ImageLike : {}", imageLikeDTO);
        ImageLike imageLike = imageLikeMapper.toEntity(imageLikeDTO);
        imageLike = imageLikeRepository.save(imageLike);
        return imageLikeMapper.toDto(imageLike);
    }

    /**
     *  Get all the imageLikes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ImageLikeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ImageLikes");
        return imageLikeRepository.findAll(pageable)
            .map(imageLikeMapper::toDto);
    }

    /**
     *  Get one imageLike by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public ImageLikeDTO findOne(Long id) {
        log.debug("Request to get ImageLike : {}", id);
        ImageLike imageLike = imageLikeRepository.findOne(id);
        return imageLikeMapper.toDto(imageLike);
    }

    /**
     *  Delete the  imageLike by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ImageLike : {}", id);
        imageLikeRepository.delete(id);
    }
}
