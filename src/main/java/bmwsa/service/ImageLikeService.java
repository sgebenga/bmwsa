package bmwsa.service;

import bmwsa.service.dto.ImageLikeDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing ImageLike.
 */
public interface ImageLikeService {

    /**
     * Save a imageLike.
     *
     * @param imageLikeDTO the entity to save
     * @return the persisted entity
     */
    ImageLikeDTO save(ImageLikeDTO imageLikeDTO);

    /**
     *  Get all the imageLikes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<ImageLikeDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" imageLike.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    ImageLikeDTO findOne(Long id);

    /**
     *  Delete the "id" imageLike.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
