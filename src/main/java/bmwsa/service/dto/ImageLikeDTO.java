package bmwsa.service.dto;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the ImageLike entity.
 */
public class ImageLikeDTO implements Serializable {

    private Long id;

    private Long userId;

    private Long imageId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getImageId() {
        return imageId;
    }

    public void setImageId(Long imageId) {
        this.imageId = imageId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ImageLikeDTO imageLikeDTO = (ImageLikeDTO) o;
        if(imageLikeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), imageLikeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ImageLikeDTO{" +
            "id=" + getId() +
            "}";
    }
}
