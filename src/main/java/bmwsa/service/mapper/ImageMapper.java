package bmwsa.service.mapper;

import bmwsa.domain.*;
import bmwsa.service.dto.ImageDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Image and its DTO ImageDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, TagMapper.class, })
public interface ImageMapper extends EntityMapper <ImageDTO, Image> {

    @Mapping(source = "poster.id", target = "posterId")
    @Mapping(source = "poster.name", target="posterName")
    ImageDTO toDto(Image image);

    @Mapping(source = "posterId", target = "poster")
    @Mapping(target = "imageLikes", ignore = true)
    Image toEntity(ImageDTO imageDTO);
    default Image fromId(Long id) {
        if (id == null) {
            return null;
        }
        Image image = new Image();
        image.setId(id);
        return image;
    }
}
