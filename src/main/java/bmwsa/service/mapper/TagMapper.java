package bmwsa.service.mapper;

import bmwsa.domain.*;
import bmwsa.service.dto.TagDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Tag and its DTO TagDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TagMapper extends EntityMapper <TagDTO, Tag> {
    
    @Mapping(target = "images", ignore = true)
    Tag toEntity(TagDTO tagDTO); 
    default Tag fromId(Long id) {
        if (id == null) {
            return null;
        }
        Tag tag = new Tag();
        tag.setId(id);
        return tag;
    }
}
