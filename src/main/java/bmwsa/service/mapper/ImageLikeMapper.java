package bmwsa.service.mapper;

import bmwsa.domain.*;
import bmwsa.service.dto.ImageLikeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ImageLike and its DTO ImageLikeDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, ImageMapper.class, })
public interface ImageLikeMapper extends EntityMapper <ImageLikeDTO, ImageLike> {

    @Mapping(source = "user.id", target = "userId")

    @Mapping(source = "image.id", target = "imageId")
    ImageLikeDTO toDto(ImageLike imageLike); 

    @Mapping(source = "userId", target = "user")

    @Mapping(source = "imageId", target = "image")
    ImageLike toEntity(ImageLikeDTO imageLikeDTO); 
    default ImageLike fromId(Long id) {
        if (id == null) {
            return null;
        }
        ImageLike imageLike = new ImageLike();
        imageLike.setId(id);
        return imageLike;
    }
}
