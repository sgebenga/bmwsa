package bmwsa.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Image.
 */
@Entity
@Table(name = "image")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Image implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Size(max = 100)
    @Column(name = "description", length = 100, nullable = false)
    private String description;

    @Column(name = "posted_date")
    private ZonedDateTime postedDate;

    @NotNull
    @Lob
    @Column(name = "content", nullable = false)
    private byte[] content;

    @Column(name = "content_content_type", nullable = false)
    private String contentContentType;

    @Min(value = 0L)
    @Column(name = "views")
    private Long views;

    @ManyToOne
    private User poster;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "image_tag",
               joinColumns = @JoinColumn(name="images_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="tags_id", referencedColumnName="id"))
    private Set<Tag> tags = new HashSet<>();

    @OneToMany(mappedBy = "image")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ImageLike> imageLikes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Image name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Image description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ZonedDateTime getPostedDate() {
        return postedDate;
    }

    public Image postedDate(ZonedDateTime postedDate) {
        this.postedDate = postedDate;
        return this;
    }

    public void setPostedDate(ZonedDateTime postedDate) {
        this.postedDate = postedDate;
    }

    public byte[] getContent() {
        return content;
    }

    public Image content(byte[] content) {
        this.content = content;
        return this;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getContentContentType() {
        return contentContentType;
    }

    public Image contentContentType(String contentContentType) {
        this.contentContentType = contentContentType;
        return this;
    }

    public void setContentContentType(String contentContentType) {
        this.contentContentType = contentContentType;
    }

    public Long getViews() {
        return views;
    }

    public Image views(Long views) {
        this.views = views;
        return this;
    }

    public void setViews(Long views) {
        this.views = views;
    }

    public User getPoster() {
        return poster;
    }

    public Image poster(User user) {
        this.poster = user;
        return this;
    }

    public void setPoster(User user) {
        this.poster = user;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public Image tags(Set<Tag> tags) {
        this.tags = tags;
        return this;
    }

    public Image addTag(Tag tag) {
        this.tags.add(tag);
        tag.getImages().add(this);
        return this;
    }

    public Image removeTag(Tag tag) {
        this.tags.remove(tag);
        tag.getImages().remove(this);
        return this;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public Set<ImageLike> getImageLikes() {
        return imageLikes;
    }

    public Image imageLikes(Set<ImageLike> imageLikes) {
        this.imageLikes = imageLikes;
        return this;
    }

    public Image addImageLike(ImageLike imageLike) {
        this.imageLikes.add(imageLike);
        imageLike.setImage(this);
        return this;
    }

    public Image removeImageLike(ImageLike imageLike) {
        this.imageLikes.remove(imageLike);
        imageLike.setImage(null);
        return this;
    }

    public void setImageLikes(Set<ImageLike> imageLikes) {
        this.imageLikes = imageLikes;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Image image = (Image) o;
        if (image.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), image.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Image{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", postedDate='" + getPostedDate() + "'" +
            ", content='" + getContent() + "'" +
            ", contentContentType='" + contentContentType + "'" +
            ", views='" + getViews() + "'" +
            "}";
    }
}
