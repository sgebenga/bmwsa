/**
 * View Models used by Spring MVC REST controllers.
 */
package bmwsa.web.rest.vm;
