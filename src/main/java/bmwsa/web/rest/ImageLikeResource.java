package bmwsa.web.rest;

import com.codahale.metrics.annotation.Timed;
import bmwsa.service.ImageLikeService;
import bmwsa.web.rest.util.HeaderUtil;
import bmwsa.web.rest.util.PaginationUtil;
import bmwsa.service.dto.ImageLikeDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ImageLike.
 */
@RestController
@RequestMapping("/api")
public class ImageLikeResource {

    private final Logger log = LoggerFactory.getLogger(ImageLikeResource.class);

    private static final String ENTITY_NAME = "imageLike";

    private final ImageLikeService imageLikeService;

    public ImageLikeResource(ImageLikeService imageLikeService) {
        this.imageLikeService = imageLikeService;
    }

    /**
     * POST  /image-likes : Create a new imageLike.
     *
     * @param imageLikeDTO the imageLikeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new imageLikeDTO, or with status 400 (Bad Request) if the imageLike has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/image-likes")
    @Timed
    public ResponseEntity<ImageLikeDTO> createImageLike(@RequestBody ImageLikeDTO imageLikeDTO) throws URISyntaxException {
        log.debug("REST request to save ImageLike : {}", imageLikeDTO);
        if (imageLikeDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new imageLike cannot already have an ID")).body(null);
        }
        ImageLikeDTO result = imageLikeService.save(imageLikeDTO);
        return ResponseEntity.created(new URI("/api/image-likes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /image-likes : Updates an existing imageLike.
     *
     * @param imageLikeDTO the imageLikeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated imageLikeDTO,
     * or with status 400 (Bad Request) if the imageLikeDTO is not valid,
     * or with status 500 (Internal Server Error) if the imageLikeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/image-likes")
    @Timed
    public ResponseEntity<ImageLikeDTO> updateImageLike(@RequestBody ImageLikeDTO imageLikeDTO) throws URISyntaxException {
        log.debug("REST request to update ImageLike : {}", imageLikeDTO);
        if (imageLikeDTO.getId() == null) {
            return createImageLike(imageLikeDTO);
        }
        ImageLikeDTO result = imageLikeService.save(imageLikeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, imageLikeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /image-likes : get all the imageLikes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of imageLikes in body
     */
    @GetMapping("/image-likes")
    @Timed
    public ResponseEntity<List<ImageLikeDTO>> getAllImageLikes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ImageLikes");
        Page<ImageLikeDTO> page = imageLikeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/image-likes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /image-likes/:id : get the "id" imageLike.
     *
     * @param id the id of the imageLikeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the imageLikeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/image-likes/{id}")
    @Timed
    public ResponseEntity<ImageLikeDTO> getImageLike(@PathVariable Long id) {
        log.debug("REST request to get ImageLike : {}", id);
        ImageLikeDTO imageLikeDTO = imageLikeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(imageLikeDTO));
    }

    /**
     * DELETE  /image-likes/:id : delete the "id" imageLike.
     *
     * @param id the id of the imageLikeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/image-likes/{id}")
    @Timed
    public ResponseEntity<Void> deleteImageLike(@PathVariable Long id) {
        log.debug("REST request to delete ImageLike : {}", id);
        imageLikeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
