package bmwsa.web.rest;

import bmwsa.BmwsaApp;

import bmwsa.domain.ImageLike;
import bmwsa.repository.ImageLikeRepository;
import bmwsa.service.ImageLikeService;
import bmwsa.service.dto.ImageLikeDTO;
import bmwsa.service.mapper.ImageLikeMapper;
import bmwsa.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ImageLikeResource REST controller.
 *
 * @see ImageLikeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BmwsaApp.class)
public class ImageLikeResourceIntTest {

    @Autowired
    private ImageLikeRepository imageLikeRepository;

    @Autowired
    private ImageLikeMapper imageLikeMapper;

    @Autowired
    private ImageLikeService imageLikeService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restImageLikeMockMvc;

    private ImageLike imageLike;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ImageLikeResource imageLikeResource = new ImageLikeResource(imageLikeService);
        this.restImageLikeMockMvc = MockMvcBuilders.standaloneSetup(imageLikeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ImageLike createEntity(EntityManager em) {
        ImageLike imageLike = new ImageLike();
        return imageLike;
    }

    @Before
    public void initTest() {
        imageLike = createEntity(em);
    }

    @Test
    @Transactional
    public void createImageLike() throws Exception {
        int databaseSizeBeforeCreate = imageLikeRepository.findAll().size();

        // Create the ImageLike
        ImageLikeDTO imageLikeDTO = imageLikeMapper.toDto(imageLike);
        restImageLikeMockMvc.perform(post("/api/image-likes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(imageLikeDTO)))
            .andExpect(status().isCreated());

        // Validate the ImageLike in the database
        List<ImageLike> imageLikeList = imageLikeRepository.findAll();
        assertThat(imageLikeList).hasSize(databaseSizeBeforeCreate + 1);
        ImageLike testImageLike = imageLikeList.get(imageLikeList.size() - 1);
    }

    @Test
    @Transactional
    public void createImageLikeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = imageLikeRepository.findAll().size();

        // Create the ImageLike with an existing ID
        imageLike.setId(1L);
        ImageLikeDTO imageLikeDTO = imageLikeMapper.toDto(imageLike);

        // An entity with an existing ID cannot be created, so this API call must fail
        restImageLikeMockMvc.perform(post("/api/image-likes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(imageLikeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ImageLike in the database
        List<ImageLike> imageLikeList = imageLikeRepository.findAll();
        assertThat(imageLikeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllImageLikes() throws Exception {
        // Initialize the database
        imageLikeRepository.saveAndFlush(imageLike);

        // Get all the imageLikeList
        restImageLikeMockMvc.perform(get("/api/image-likes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(imageLike.getId().intValue())));
    }

    @Test
    @Transactional
    public void getImageLike() throws Exception {
        // Initialize the database
        imageLikeRepository.saveAndFlush(imageLike);

        // Get the imageLike
        restImageLikeMockMvc.perform(get("/api/image-likes/{id}", imageLike.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(imageLike.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingImageLike() throws Exception {
        // Get the imageLike
        restImageLikeMockMvc.perform(get("/api/image-likes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateImageLike() throws Exception {
        // Initialize the database
        imageLikeRepository.saveAndFlush(imageLike);
        int databaseSizeBeforeUpdate = imageLikeRepository.findAll().size();

        // Update the imageLike
        ImageLike updatedImageLike = imageLikeRepository.findOne(imageLike.getId());
        ImageLikeDTO imageLikeDTO = imageLikeMapper.toDto(updatedImageLike);

        restImageLikeMockMvc.perform(put("/api/image-likes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(imageLikeDTO)))
            .andExpect(status().isOk());

        // Validate the ImageLike in the database
        List<ImageLike> imageLikeList = imageLikeRepository.findAll();
        assertThat(imageLikeList).hasSize(databaseSizeBeforeUpdate);
        ImageLike testImageLike = imageLikeList.get(imageLikeList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingImageLike() throws Exception {
        int databaseSizeBeforeUpdate = imageLikeRepository.findAll().size();

        // Create the ImageLike
        ImageLikeDTO imageLikeDTO = imageLikeMapper.toDto(imageLike);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restImageLikeMockMvc.perform(put("/api/image-likes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(imageLikeDTO)))
            .andExpect(status().isCreated());

        // Validate the ImageLike in the database
        List<ImageLike> imageLikeList = imageLikeRepository.findAll();
        assertThat(imageLikeList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteImageLike() throws Exception {
        // Initialize the database
        imageLikeRepository.saveAndFlush(imageLike);
        int databaseSizeBeforeDelete = imageLikeRepository.findAll().size();

        // Get the imageLike
        restImageLikeMockMvc.perform(delete("/api/image-likes/{id}", imageLike.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ImageLike> imageLikeList = imageLikeRepository.findAll();
        assertThat(imageLikeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ImageLike.class);
        ImageLike imageLike1 = new ImageLike();
        imageLike1.setId(1L);
        ImageLike imageLike2 = new ImageLike();
        imageLike2.setId(imageLike1.getId());
        assertThat(imageLike1).isEqualTo(imageLike2);
        imageLike2.setId(2L);
        assertThat(imageLike1).isNotEqualTo(imageLike2);
        imageLike1.setId(null);
        assertThat(imageLike1).isNotEqualTo(imageLike2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ImageLikeDTO.class);
        ImageLikeDTO imageLikeDTO1 = new ImageLikeDTO();
        imageLikeDTO1.setId(1L);
        ImageLikeDTO imageLikeDTO2 = new ImageLikeDTO();
        assertThat(imageLikeDTO1).isNotEqualTo(imageLikeDTO2);
        imageLikeDTO2.setId(imageLikeDTO1.getId());
        assertThat(imageLikeDTO1).isEqualTo(imageLikeDTO2);
        imageLikeDTO2.setId(2L);
        assertThat(imageLikeDTO1).isNotEqualTo(imageLikeDTO2);
        imageLikeDTO1.setId(null);
        assertThat(imageLikeDTO1).isNotEqualTo(imageLikeDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(imageLikeMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(imageLikeMapper.fromId(null)).isNull();
    }
}
