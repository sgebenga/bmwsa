/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { BmwsaTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { ImageLikeDetailComponent } from '../../../../../../main/webapp/app/entities/image-like/image-like-detail.component';
import { ImageLikeService } from '../../../../../../main/webapp/app/entities/image-like/image-like.service';
import { ImageLike } from '../../../../../../main/webapp/app/entities/image-like/image-like.model';

describe('Component Tests', () => {

    describe('ImageLike Management Detail Component', () => {
        let comp: ImageLikeDetailComponent;
        let fixture: ComponentFixture<ImageLikeDetailComponent>;
        let service: ImageLikeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BmwsaTestModule],
                declarations: [ImageLikeDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    ImageLikeService,
                    JhiEventManager
                ]
            }).overrideTemplate(ImageLikeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ImageLikeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ImageLikeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new ImageLike(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.imageLike).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
